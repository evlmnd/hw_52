import React, { Component } from 'react';
import Number from './components/Number/Number';

import './App.css';

class App extends Component {

    state = {
        numbers: [5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36],
        randomNumbers: ['x','x','x','x','x']
    };

    changeNumbers = () => {
        const numbers = [...this.state.numbers];

        const pickedNumbers = [];
        for (let i = 0; i < 5; i++) {
            let randomNumber = numbers[Math.floor(Math.random()*numbers.length)];
            pickedNumbers.push(randomNumber);
            numbers.splice(randomNumber - 5, 1);
        }

        pickedNumbers.sort((a, b) => a - b);  //https://stackoverflow.com/questions/1063007/how-to-sort-an-array-of-integers-correctly
        this.setState({randomNumbers: pickedNumbers})
    };

    render() {
        return(
            <div className="App">
                <button className="button" onClick={this.changeNumbers}>New Numbers</button>
                <div className="numbers">
                    {this.state.randomNumbers.map((number, index) => {
                        return (
                            <Number key={index} number={number}/>
                        )
                    })}
                </div>
            </div>
        )
    }
}

export default App;
