import React, { Component } from 'react';

import './Number.css';

class Number extends Component {
    render() {
        return (
            <div className="number">
                {this.props.number}
            </div>
        )
    }
}

export default Number;